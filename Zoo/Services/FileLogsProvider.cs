﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Services
{
    public class FileLogsProvider : ILogsProvider
    {
        protected string exceptionsFileName;
        protected string nl = Environment.NewLine;

        public FileLogsProvider(IConfiguration configuration) => exceptionsFileName = configuration.GetSection("Logs").GetValue<string>("Exceptions");

        public async Task LogException(Exception exception)
        {
            using (StreamWriter writer = new StreamWriter(new FileStream(exceptionsFileName, FileMode.Append
                , FileAccess.Write)))
                await writer.WriteLineAsync(GetText(exception));
        }

        public string GetText(Exception exception)
            => $"Unhandled exception occured @ {DateTime.Now} Source : {exception.Source}" + nl 
            + $"{exception.ToString()}" + nl + nl + nl;
    }
}
