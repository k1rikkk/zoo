﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;

namespace Zoo.Components
{
    public class Navbar : ViewComponent
    {
        protected AppDbContext dbContext;

        public Navbar(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<Family> families = await dbContext.Families.ToListAsync();
            ViewBag.Adding = HttpContext.User.Identity.IsAuthenticated;
            return View("/Views/Shared/Navbar.cshtml", families);
        }
    }
}
