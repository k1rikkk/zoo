﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;

namespace Zoo.Components
{
    public class Footer : ViewComponent
    {
        protected AppDbContext dbContext;

        public Footer(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            List<Family> families = await dbContext.Families.ToListAsync();
            return View("/Views/Shared/Footer.cshtml", families);
        }
    }
}
