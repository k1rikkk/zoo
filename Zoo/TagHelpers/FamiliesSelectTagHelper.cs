﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data;
using Zoo.Data.Models;

namespace Zoo.TagHelpers
{
    public class FamiliesSelectTagHelper : TagHelper
    {
        protected AppDbContext dbContext;

        public string Name { get; set; }
        public string Class { get; set; }
        public int Selected { get; set; }

        public FamiliesSelectTagHelper(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "select";
            output.Attributes.SetAttribute("name", Name);
            output.Attributes.SetAttribute("class", " " + Class);
            string content = "";
            List<Family> families = await dbContext.Families.ToListAsync();
            foreach (Family family in families)
                content += $"<option value=\"{family.Id}\" {(Selected == family.Id ? "selected" : "")}>"
                    + $"{family.Name}</option>";
            output.Content.SetHtmlContent(content);
        }
    }
}
