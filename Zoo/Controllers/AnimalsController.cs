﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;
using Zoo.Filters;

namespace Zoo.Controllers
{
    public class AnimalsController : Controller
    {
        protected AppDbContext dbContext;
        protected int animalsPerPage;
        protected int imagesPerPage;

        public AnimalsController(AppDbContext dbContext, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            IConfigurationSection browse = configuration.GetSection("Browse");
            animalsPerPage = browse.GetValue<int>("Animals per page");
            imagesPerPage = browse.GetValue<int>("Animal images per page");
        }

        public async Task<IActionResult> All(int id = 1)
        {
            ViewBag.Title = "Zoo | All animals | " + id;
            PagedVM<List<Animal>> vm = new PagedVM<List<Animal>>
            {
                Pages = new PagesVM()
            };
            vm.Pages.Total = Extensions.GetTotalPages(await dbContext.Animals.CountAsync(), animalsPerPage);
            id = Extensions.PageNormalized(id, vm.Pages.Total);
            vm.Pages.Current = id;
            vm.Pages.Href = "/Animals/All/{page}";
            vm.Data = await dbContext.Animals.Skip(animalsPerPage * (id - 1)).Take(animalsPerPage)
                .Include(a => a.Images).Include(a => a.Family).OrderBy(a => a.Name).ToListAsync();
            return View(vm);
        }

        [Route("Animals/Details/{id}")]
        [Route("Animals/Details/{id}/{page}")]
        public async Task<IActionResult> Details(int id = -1, int page = 1)
        {
            Animal animal = await dbContext.Animals.Where(a => a.Id == id)
                .Include(a => a.Family).FirstOrDefaultAsync();
            if (animal == null)
                return StatusCode(404);
            AnimalDetailsVM vm = new AnimalDetailsVM
            {
                Pages = new PagesVM(),
                Data = animal,
                CanEdit = HttpContext.User.Identity.IsAuthenticated
            };
            vm.Pages.Total = Extensions.GetTotalPages(await dbContext.Images.Where(i => i.Id == animal.Id)
                .CountAsync(), imagesPerPage);
            page = Extensions.PageNormalized(page, vm.Pages.Total);
            vm.Pages.Current = page;
            vm.Pages.Href = "/Animals/Details/" + animal.Id + "/{page}";
            animal.Images = await dbContext.Images.Where(i => i.AnimalId == animal.Id)
                .Skip(imagesPerPage * (page - 1)).Take(imagesPerPage)
                .ToListAsync();
            vm.Other = await dbContext.Animals.FromSql("SELECT * FROM Animals WHERE Id IN (SELECT Id FROM "
                + $"Animals WHERE FamilyId = {animal.FamilyId} AND Id != {animal.Id} ORDER BY RANDOM() LIMIT 2)")
                .ToListAsync();
            foreach (Animal a in vm.Other)
            {
                a.Images = new List<Image>();
                Image image = await dbContext.Images.FirstOrDefaultAsync(i => i.AnimalId == a.Id);
                if (image != null)
                    a.Images.Add(image);
            }
            ViewBag.Title = $"Zoo | {animal.Name} | {page}";
            return View(vm);
        }

        [HttpGet]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(int id = -1)
        {
            Animal animal = await dbContext.Animals.FirstOrDefaultAsync(a => a.Id == id);
            if (animal == null)
                return StatusCode(404);
            ViewBag.Title = "Zoo | Edit animal | " + id;
            ViewBag.Adding = false;
            return View(animal);
        }

        [HttpGet]
        [AuthenticatedFilter]
        public IActionResult Add()
        {
            ViewBag.Title = "Zoo | Add animal";
            ViewBag.Adding = true;
            return View("Edit", null);
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(Animal animal)
        {
            ViewBag.Adding = false;
            ViewBag.Title = "Zoo | Edit animal | " + animal.Id;
            if (!ModelState.IsValid)
                return View(animal);
            dbContext.Animals.Update(animal);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = animal.Id });
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Add(Animal animal)
        {
            ViewBag.Adding = true;
            ViewBag.Title = "Zoo | Add animal";
            if (!ModelState.IsValid)
                return View("Edit", animal);
            await dbContext.Animals.AddAsync(animal);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = animal.Id });
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Remove(int id = -1)
        {
            Animal animal = await dbContext.Animals.FirstOrDefaultAsync(a => a.Id == id);
            if (animal == null)
                return StatusCode(404);
            List<Image> images = await dbContext.Images.Where(i => i.AnimalId == animal.Id).ToListAsync();
            foreach (Image image in images)
                image.AnimalId = null;
            dbContext.Images.UpdateRange(images);
            dbContext.Animals.Remove(animal);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AuthenticatedFilter]
        public async Task<IActionResult> EditImages(int id = -1)
        {
            Animal animal = await dbContext.Animals.Where(a => a.Id == id).Include(a => a.Images).FirstOrDefaultAsync();
            if (animal == null)
                return StatusCode(404);
            List<Image> images = animal.Images;
            ViewBag.Title = "Zoo | Edit photos | " + animal.Name;
            AnimalEditImagesVM vm = new AnimalEditImagesVM
            {
                Animal = animal,
                Images = images
            };
            return View(vm);
        }
        
        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> RemoveImage(int id)
        {
            Image image = await dbContext.Images.FirstOrDefaultAsync(i => i.Id == id);
            if (image == null)
                return StatusCode(404);
            int animalId = image.AnimalId.Value;
            image.AnimalId = null;
            dbContext.Images.Update(image);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("EditImages", new { id = animalId });
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> AddImage(int id, int imageId)
        {
            Animal animal = await dbContext.Animals.FirstOrDefaultAsync(a => a.Id == id);
            Image image = await dbContext.Images.FirstOrDefaultAsync(i => i.Id == imageId);
            if ((animal == null) || (image == null))
                return StatusCode(404);
            image.AnimalId = animal.Id;
            dbContext.Images.Update(image);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("EditImages", new { id });
        }
    }
}