﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;
using Zoo.Filters;

namespace Zoo.Controllers
{
    public class FamiliesController : Controller
    {
        protected AppDbContext dbContext;
        protected int animalsPerPage;

        public FamiliesController(AppDbContext dbContext, IConfiguration configuration)
        {
            this.dbContext = dbContext;
            animalsPerPage = configuration.GetSection("Browse").GetValue<int> ("Family animals per page");
        }

        [Route("Families/Details/{id}")]
        [Route("Families/Details/{id}/{page}")]
        public async Task<IActionResult> Details(int id = -1, int page = 1)
        {
            Family family = await dbContext.Families.FirstOrDefaultAsync(f => f.Id == id);
            if (family == null)
                return StatusCode(404);
            PagedVM<Family> vm = new PagedVM<Family>
            {
                Pages = new PagesVM(),
                CanEdit = HttpContext.User.Identity.IsAuthenticated
            };
            vm.Pages.Href = "/Families/Details/" + id + "/{page}";
            IQueryable<Animal> queryable = dbContext.Animals.Where(a => a.FamilyId == family.Id)
                .OrderBy(a => a.Name);
            vm.Pages.Total = Extensions.GetTotalPages(await queryable.CountAsync(), animalsPerPage);
            page = Extensions.PageNormalized(page, vm.Pages.Total);
            vm.Pages.Current = page;
            family.Animals = await queryable.Skip(animalsPerPage * (page - 1)).Take(animalsPerPage)
                .Include(a => a.Images).ToListAsync();
            ViewBag.Title = "Zoo | " + family.Name + " | " + page;
            vm.Data = family;
            return View(vm);
        }

        [HttpGet]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(int id = -1)
        {
            Family family = await dbContext.Families.FirstOrDefaultAsync(f => f.Id == id);
            if (family == null)
                return StatusCode(404);
            ViewBag.Title = "Zoo | Edit family | " + id;
            ViewBag.Adding = false;
            return View(family);
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(Family family)
        {
            ViewBag.Title = "Zoo | Edit family | " + family.Id;
            ViewBag.Adding = false;
            if (!ModelState.IsValid)
                return View(family);
            dbContext.Families.Update(family);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = family.Id });
        }

        [HttpGet]
        [AuthenticatedFilter]
        public IActionResult Add()
        {
            ViewBag.Title = "Zoo | Add family";
            ViewBag.Adding = true;
            return View("Edit", null);
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Add(Family family)
        {
            ViewBag.Title = "Zoo | Add family";
            ViewBag.Adding = true;
            if (!ModelState.IsValid)
                return View("Edit", family);
            await dbContext.Families.AddAsync(family);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = family.Id });
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Remove(int id = -1)
        {
            Family family = await dbContext.Families.Where(f => f.Id == id).Include(f => f.Animals)
                .FirstOrDefaultAsync();
            if (family == null)
                return StatusCode(404);
            dbContext.RemoveRange(family.Animals);
            dbContext.Remove(family);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}