﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;
using Zoo.Filters;
using Zoo.Services;

namespace Zoo.Controllers
{
    public class HomeController : Controller
    {
        protected AppDbContext dbContext;
        protected string adminKey;
        protected string webRoot;
        protected int galleryImagesPerPage;
        protected int searchItemsPerPage;

        public HomeController(AppDbContext dbContext, IConfiguration configuration, IHostingEnvironment env)
        {
            this.dbContext = dbContext;
            adminKey = configuration.GetSection("Admin").GetValue<string>("Key");
            webRoot = env.WebRootPath;
            IConfigurationSection browse = configuration.GetSection("Browse");
            galleryImagesPerPage = browse.GetValue<int>("Gallery images per page");
            searchItemsPerPage = browse.GetValue<int>("Search items per page");
        }

        public async Task<IActionResult> Index()
        {
            HomeVM vm = new HomeVM
            {
                New = await dbContext.Animals.OrderByDescending(a => a.PostedOn).Take(2).Include(a => a.Images)
                    .Include(a => a.Family).ToListAsync(),
                TopMonth = await GetTopMonth()
            };
            ViewBag.Title = "Zoo | Home";
            return View(vm);
        }
        
        public async Task<List<Animal>> GetTopMonth() 
        {
            List<Animal> ret = new List<Animal>();
            int[] ids = new int[] { 3, 5, 7, 4, 6, 1 };
            foreach (int id in ids)
                ret.Add(await dbContext.Animals.Where(a => a.Id == id).Include(a => a.Images).Include(a => a.Family)
                    .FirstAsync());
            return ret;
        }

        [HttpGet]
        [HttpPost]
        public async Task<IActionResult> Search(string search, int animalsPage = 1, int familiesPage = 1
            , int imagesPage = 1)
        {
            string[] tags = search?.Split(new string[] { " , ", ", ", " ,", ",", " " }
                , StringSplitOptions.RemoveEmptyEntries) ?? new string[] { "" };
            Task<PagedVM<List<Animal>>> ans = FindAnimals(tags, animalsPage);
            Task<PagedVM<List<Family>>> fms = FindFamilies(tags, familiesPage);
            Task<PagedVM<List<Image>>> ims = FindImages(tags, imagesPage);
            await Task.WhenAll(ans, fms, ims);
            SearchVM vm = new SearchVM
            {
                Animals = ans.Result,
                Families = fms.Result,
                Images = ims.Result
            };
            vm.Animals.Pages.Href = "/Home/Search?search=" + search + "&animalsPage={page}&familiesPage="
                + familiesPage + "&imagesPage=" + imagesPage + "#animals";
            vm.Families.Pages.Href = "/Home/Search?search=" + search + "&animalsPage=" + animalsPage
                + "&familiesPage={page}&imagesPage=" + imagesPage + "#families";
            vm.Images.Pages.Href = "/Home/Search?search=" + search + "&animalsPage=" + animalsPage
                + "&familiesPage=" + familiesPage + "&imagesPage={page}#images";
            vm.Search = search;
            ViewBag.Title = "Zoo | Search";
            return View(vm);
        }

        protected async Task<PagedVM<List<Animal>>> FindAnimals(string[] tags, int page)
        {
            string sql = GetSqlDescribed("Animals", tags);
            IQueryable<Animal> queryable = dbContext.Animals.FromSql(sql).OrderBy(a => a.Name);
            int totalPages = Extensions.GetTotalPages(await queryable.CountAsync(), searchItemsPerPage);
            page = Extensions.PageNormalized(page, totalPages);
            List<Animal> animals = await queryable.Skip(searchItemsPerPage * (page - 1)).Take(searchItemsPerPage)
                .ToListAsync();
            foreach (Animal animal in animals)
            {
                animal.Images = await dbContext.Images.Where(i => i.AnimalId == animal.Id).ToListAsync();
                animal.Family = await dbContext.Families.FirstAsync(f => f.Id == animal.FamilyId);
            }
            return new PagedVM<List<Animal>>
            {
                Data = animals,
                Pages = new PagesVM
                {
                    Current = page,
                    Total = totalPages
                }
            };
        }

        protected async Task<PagedVM<List<Family>>> FindFamilies(string[] tags, int page)
        {
            string sql = GetSqlDescribed("Families", tags);
            IQueryable<Family> queryable = dbContext.Families.FromSql(sql).OrderBy(a => a.Name);
            int totalPages = Extensions.GetTotalPages(await queryable.CountAsync(), searchItemsPerPage);
            page = Extensions.PageNormalized(page, totalPages);
            List<Family> families = await queryable.Skip(searchItemsPerPage * (page - 1)).Take(searchItemsPerPage)
                .ToListAsync();
            return new PagedVM<List<Family>>
            {
                Data = families,
                Pages = new PagesVM
                {
                    Current = page,
                    Total = totalPages
                }
            };
        }

        protected string GetSqlDescribed(string table, string[] tags)
        {
            string sql = $"SELECT * FROM {table} WHERE (";
            for (int i = 0; i < tags.Length - 1; i++)
                sql += $"Name LIKE \'%{tags[i]}%\' AND ";
            sql += $"Name LIKE \'%{tags.Last()}%\') OR (";
            for (int i = 0; i < tags.Length - 1; i++)
                sql += $"Description LIKE \'%{tags[i]}%\' AND ";
            sql += $"Description LIKE \'%{tags.Last()}%\'";
            sql += ")";
            return sql;
        }

        protected async Task<PagedVM<List<Image>>> FindImages(string[] tags, int page)
        {
            string sql = "SELECT * FROM Images WHERE(";
            for (int i = 0; i < tags.Length - 1; i++)
                sql += $"Title LIKE \'%{tags[i]}%\' AND ";
            sql += $"Title LIKE \'%{tags.Last()}%\')";
            IQueryable<Image> queryable = dbContext.Images.FromSql(sql).OrderByDescending(i => i.DateTime);
            int totalPages = Extensions.GetTotalPages(await queryable.CountAsync(), searchItemsPerPage);
            page = Extensions.PageNormalized(page, totalPages);
            List<Image> images = await queryable.Skip(searchItemsPerPage * (page - 1)).Take(searchItemsPerPage)
                .ToListAsync();
            return new PagedVM<List<Image>>
            {
                Data = images,
                Pages = new PagesVM
                {
                    Current = page,
                    Total = totalPages
                }
            };
        }

        [HttpGet]
        [Route("/Home/Login")]
        [Route("/Home/Admin")]
        [Route("/Admin")]
        [Route("/Admin/Login")]
        public IActionResult Login() => View();

        [HttpPost]
        public async Task<IActionResult> Login(string id)
        {
            if (id != adminKey)
            {
                ViewBag.Message = "Wrong key";
                return View();
            }
            List<Claim> claims = new List<Claim> { new Claim(ClaimsIdentity.DefaultNameClaimType, "Admin") };
            ClaimsIdentity identity = new ClaimsIdentity(claims, "ApplicationCookie"
                , ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                new ClaimsPrincipal(identity));
            return RedirectToAction("Index");
        }

        [Route("/Home/Logout")]
        [Route("/Home/Admin/Logout")]
        [Route("/Logout")]
        [Route("/Admin/Logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index");
        }
        
        public IActionResult Exception([FromServices] ILogsProvider provider)
        {
            IExceptionHandlerFeature feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
            Exception error = feature?.Error;
            try { provider.LogException(error); }
            catch (Exception) { }
            return View(error);
        }

        [Route("/Contacts")]
        public IActionResult Contacts()
        {
            ViewBag.Title = "Zoo | Contacts";
            return View();
        }

        [Route("/About")]
        public IActionResult About()
        {
            ViewBag.Title = "Zoo | About";
            return View();
        }
    }
}