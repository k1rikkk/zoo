﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Zoo.Data;
using Zoo.Data.Models;
using Zoo.Data.ViewModels;
using Zoo.Filters;

namespace Zoo.Controllers
{
    public class GalleryController : Controller
    {
        protected AppDbContext dbContext;
        protected string webRoot;
        protected int galleryImagesPerPage;

        public GalleryController(AppDbContext dbContext, IConfiguration configuration, IHostingEnvironment env)
        {
            this.dbContext = dbContext;
            webRoot = env.WebRootPath;
            IConfigurationSection browse = configuration.GetSection("Browse");
            galleryImagesPerPage = browse.GetValue<int>("Gallery images per page");
        }

        public async Task<IActionResult> Index(int id = 1)
        {
            ViewBag.Title = "Zoo | Gallery | 1";
            id = id < 1 ? 1 : id;
            PagedVM<List<Image>> vm = new PagedVM<List<Image>>
            {
                Pages = new PagesVM()
            };
            vm.Pages.Total = Extensions.GetTotalPages(await dbContext.Images.CountAsync(), galleryImagesPerPage);
            id = Extensions.PageNormalized(id, vm.Pages.Total);
            vm.Data = await dbContext.Images.Skip(galleryImagesPerPage * (id - 1)).Take(galleryImagesPerPage).ToListAsync();
            vm.Pages.Current = id;
            vm.Pages.Href = "/Home/Gallery/{page}";
            return View(vm);
        }

        [HttpGet]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(int id = -1)
        {
            Image image = await dbContext.Images.FirstOrDefaultAsync(i => i.Id == id);
            if (image == null)
                return StatusCode(404);
            ViewBag.Title = "Zoo | Edit photo | " + id;
            ViewBag.Adding = false;
            return View(image);
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Edit(IFormFile file, Image image)
        {
            ViewBag.Adding = false;
            ViewBag.Title = "Zoo | Edit photo | " + image.Id;
            string directory = webRoot + "/" + HttpContext.GetImagesPath();
            string fileName = Path.GetFileName(file?.FileName) ?? "";
            if ((file != null) && (System.IO.File.Exists(directory + fileName)))
                ModelState.AddModelError("file", "File with this name already exists");
            if (!ModelState.IsValid)
                return View(image);
            if (file != null)
                using (FileStream fs = new FileStream(directory + fileName, FileMode.Create, FileAccess.Write))
                {
                    image.Path = fileName;
                    await file.CopyToAsync(fs);
                }
            dbContext.Images.Update(image);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = image.Id });
        }

        [HttpGet]
        [AuthenticatedFilter]
        public IActionResult Add()
        {
            ViewBag.Title = "Zoo | Add photo";
            ViewBag.Adding = true;
            return View("Edit", null);
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> Add(Image image, IFormFile file)
        {
            ViewBag.Title = "Zoo | Add photo";
            ViewBag.Adding = true;
            string directory = webRoot + "/" + HttpContext.GetImagesPath();
            string fileName = Path.GetFileName(file?.FileName) ?? "";
            image.Path = fileName;
            if (file == null)
                ModelState.AddModelError("file", "Select file");
            if (System.IO.File.Exists(directory + fileName))
                ModelState.AddModelError("file", "File with this name already exists");
            if (!ModelState.IsValid)
                return View("Edit", image);
            using (FileStream fs = new FileStream(directory + fileName, FileMode.Create, FileAccess.Write))
                await file.CopyToAsync(fs);
            await dbContext.Images.AddAsync(image);
            await dbContext.SaveChangesAsync();
            return RedirectToAction("Edit", new { id = image.Id });
        }

        [HttpPost]
        [AuthenticatedFilter]
        public async Task<IActionResult> RemovePhoto(int id = -1)
        {
            Image image = await dbContext.Images.FirstOrDefaultAsync(i => i.Id == id);
            if (image == null)
                return StatusCode(404);
            dbContext.Images.Remove(image);
            await dbContext.SaveChangesAsync();
            System.IO.File.Delete(webRoot + "/" + HttpContext.GetImagesPath() + image.Path);
            return RedirectToAction("Index");
        }
    }
}