﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.Models
{
    public class Family : DescribedModel
    {
        public virtual List<Animal> Animals { get; set; }
    }
}
