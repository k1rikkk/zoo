﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.Models
{
    public class Animal : DescribedModel
    {
        [Required]
        [DataType(DataType.Date)]
        public DateTime PostedOn { get; set; }
        [Required]
        public bool IsNew { get; set; }
        public int FamilyId { get; set; }

        public virtual Family Family { get; set; }
        public virtual List<Image> Images { get; set; }

        [NotMapped]
        public Image CardImage => (Images ?? new List<Image>()).Count > 0 ? Images.First() : new Image
            {
                AnimalId = 0,
                Animal = this,
                Id = 0,
                DateTime = DateTime.Now,
                Title = "No photo",
                Path = "default.png"
            };
    }
}
