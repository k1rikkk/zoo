﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.Models
{
    public abstract class DescribedModel : Model
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }

        [NotMapped]
        public virtual string ShortDescription => (Description?.Length ?? 0) < 100 ? Description ?? ""
            : Description.Substring(0, 98) + "..";
    }
}
