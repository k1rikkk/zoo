﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.Models
{
    public class Image : Model
    {
        [Required]
        [MaxLength(100)]
        public string Path { get; set; }
        [MaxLength(200)]
        public string Title { get; set; }
        public DateTime DateTime { get; set; }
        public int? AnimalId { get; set; }

        public virtual Animal Animal { get; set; }
    }
}
