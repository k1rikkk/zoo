﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.ViewModels
{
    public class RemoveDialogVM
    {
        public string FormAction { get; set; }
        public string BodyText { get; set; }
    }
}
