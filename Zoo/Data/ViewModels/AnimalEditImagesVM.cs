﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo.Data.ViewModels
{
    public class AnimalEditImagesVM
    {
        public List<Image> Images { get; set; }
        public Animal Animal { get; set; }
    }
}
