﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo.Data.ViewModels
{
    public class AnimalDetailsVM : PagedVM<Animal>
    {
        public List<Animal> Other { get; set; }
    }
}
