﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo.Data.ViewModels
{
    public class HomeVM
    {
        public List<Animal> TopMonth { get; set; }
        public List<Animal> New { get; set; }
    }
}
