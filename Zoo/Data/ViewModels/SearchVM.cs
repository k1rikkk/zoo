﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo.Data.ViewModels
{
    public class SearchVM
    {
        public PagedVM<List<Image>> Images { get; set; }
        public PagedVM<List<Animal>> Animals { get; set; }
        public PagedVM<List<Family>> Families { get; set; }
        public string Search { get; set; }

        public SearchVM()
        {
            Images = new PagedVM<List<Image>>();
            Images.Pages = new PagesVM();
            Animals = new PagedVM<List<Animal>>();
            Animals.Pages = new PagesVM();
            Families = new PagedVM<List<Family>>();
            Families.Pages = new PagesVM();
        }
    }
}
