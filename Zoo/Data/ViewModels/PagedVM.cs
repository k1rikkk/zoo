﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.ViewModels
{
    public class PagedVM<T>
    {
        public T Data { get; set; }
        public PagesVM Pages { get; set; }
        public bool CanEdit { get; set; }
    }
}
