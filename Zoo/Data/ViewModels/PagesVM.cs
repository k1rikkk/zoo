﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zoo.Data.ViewModels
{
    public class PagesVM
    {
        public int Current { get; set; }
        public int Total { get; set; }
        /// <summary>
        /// Format: /SomeUrl/Action/{page}?param=value. {page} will be replaced.
        /// </summary>
        public string Href { get; set; }
        
        public List<int> Pages
        {
            get
            {
                List<int> list = new List<int>();
                if (Current != 1)
                    list.Add(1);
                for (int i = Current - 1; (i > 1) && (Current - 3 < i); i--)
                    list.Add(i);
                list.Add(Current);
                for (int i = Current + 1; (i < Total) && (Current + 3 > i); i++)
                    list.Add(i);
                if ((Current != Total) && (Total != int.MaxValue) && (Total != 0))
                    list.Add(Total);
                return list;
            }
        }
    }
}
