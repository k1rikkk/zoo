﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo.Data
{
    public class AppDbContext : DbContext
    {
        public DbSet<Family> Families { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Image> Images { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }
    }
}
