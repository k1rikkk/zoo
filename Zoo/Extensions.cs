﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Zoo.Data.Models;

namespace Zoo
{
    public static class Extensions
    {
        public static string GetImagesPath(this HttpContext httpContext) 
            => ((IConfiguration)httpContext.RequestServices.GetService(typeof(IConfiguration)))
                .GetSection("Browse").GetValue<string>("Images directory");

        public static bool IsAdmin(this HttpContext httpContext) 
            => httpContext.User.Identity.IsAuthenticated;

        public static int GetTotalPages(int count, int perPage)
            => (int)Math.Ceiling((float)count / perPage);

        public static int PageNormalized(int page, int total)
        {
            page = page > total ? total : page;
            page = page < 1 ? 1 : page;
            return page;
        }

        public static IWebHostBuilder UseUrlFromConfig(this IWebHostBuilder host)
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();
            host.UseUrls(config.GetValue<string>("Url"));
            return host;
        }
    }
}
