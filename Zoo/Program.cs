﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Zoo.Data;
using Zoo.Data.Models;

namespace Zoo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost host = BuildWebHost(args);
            using (IServiceScope scope = ((IServiceScopeFactory)host.Services
                .GetService(typeof(IServiceScopeFactory))).CreateScope())
            {
                AppDbContext dbContext = scope.ServiceProvider.GetService<AppDbContext>();
                dbContext.Database.EnsureCreated();
                if (((IConfiguration)host.Services.GetService(typeof(IConfiguration))).GetValue<bool>("Seed"))
                    Seed(dbContext);
            }
            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseKestrel()
                .UseIISIntegration()
                //.UseUrlFromConfig() 
                .Build();

        public static void Seed(AppDbContext dbContext)
        {
            if ((dbContext.Animals.Count() != 0) || (dbContext.Images.Count() != 0))
                return;
            Family f1 = new Family
            {
                Name = "Birds",
                Description = "Birds are a group of endothermic vertebrates, characterised by feathers, toothless beaked jaws, the laying of hard-shelled eggs, a high metabolic rate, a four-chambered heart, and a strong yet lightweight skeleton. Birds live worldwide and range in size from the 5 cm (2 in) bee hummingbird to the 2.75 m (9 ft) ostrich. They rank as the class of tetrapods with the most living species, at approximately ten thousand, with more than half of these being passerines, sometimes known as perching birds. Birds are the closest living relatives of crocodilians. They are descendants of extinct dinosaurs with feathers, making them the only surviving dinosaurs according to cladistics."
            };
            Family f2 = new Family
            {
                Name = "Mammals",
                Description = "Mammals are any vertebrates within the class Mammalia, a clade of endothermic amniotes distinguished from reptiles (including birds) by the possession of a neocortex (a region of the brain), hair, three middle ear bones, and mammary glands. Females of all mammal species nurse their young with milk, secreted from the mammary glands."
            };
            Family f3 = new Family
            {
                Name = "Rodents",
                Description = "Rodents are mammals of the order Rodentia, which are characterized by a single pair of continuously growing incisors in each of the upper and lower jaws. About 40% of all mammal species are rodents; they are found in vast numbers on all continents except Antarctica. They are the most diversified mammalian order and live in a variety of terrestrial habitats, including human-made environments."
            };
            dbContext.Families.AddRange(f1, f2, f3);
            dbContext.SaveChanges();
            Animal a1 = new Animal
            {
                Description = "Foxes are small-to-medium-sized, omnivorous mammals belonging to several genera of the family Canidae. Foxes have a flattened skull, upright triangular ears, a pointed, slightly upturned snout, and a long bushy tail (or brush).",
                Family = f2,
                Name = "Foxes",
                PostedOn = DateTime.Parse("2019-01-19 19:26")
            };
            Animal a2 = new Animal
            {
                Description = "Characteristic features of parrots include a strong, curved bill, an upright stance, strong legs, and clawed zygodactyl feet. Many parrots are vividly coloured, and some are multi-coloured. Most parrots exhibit little or no sexual dimorphism in the visual spectrum. They form the most variably sized bird order in terms of length. The most important components of most parrots diets are seeds, nuts, fruit, buds, and other plant material. A few species sometimes eat animals and carrion, while the lories and lorikeets are specialised for feeding on floral nectar and soft fruits. Almost all parrots nest in tree hollows (or nest boxes in captivity), and lay white eggs from which hatch altricial (helpless) young.",
                Family = f1,
                Name = "Parrots",
                PostedOn = DateTime.Parse("2019-01-19 19:29")
            };
            Animal a3 = new Animal
            {
                Description = "Owls are birds typified by an upright stance, a large, broad head, binocular vision, binaural hearing, sharp talons, and feathers adapted for silent flight. Exceptions include the diurnal northern hawk-owl and the gregarious burrowing owl. Owls hunt mostly small mammals, insects, and other birds, although a few species specialize in hunting fish. They are found in all regions of the Earth except Antarctica and some remote islands.",
                Family = f1,
                Name = "Owls",
                PostedOn = DateTime.Parse("2019-01-19 19:31")
            };
            Animal a4 = new Animal
            {
                Description = "Squirrels live in almost every habitat from tropical rainforest to semiarid desert, avoiding only the high polar regions and the driest of deserts. They are predominantly herbivorous, subsisting on seeds and nuts, but many will eat insects and even small vertebrates.",
                Family = f3,
                Name = "Squirrels",
                PostedOn = DateTime.Parse("2019-01-19 19:32")
            };
            Animal a5 = new Animal
            {
                Description = "Brown and American black bears are generally diurnal, meaning that they are active for the most part during the day, though they may forage substantially by night. Other species may be nocturnal, active at night, though female sloth bears with cubs may feed more at daytime to avoid competition from conspecifics and nocturnal predators. Bears are overwhelmingly solitary and are considered to be the most asocial of all the Carnivora. The only times bears are encountered in small groups are mothers with young or occasional seasonal bounties of rich food (such as salmon runs).",
                Family = f2,
                Name = "Bears",
                PostedOn = DateTime.Parse("2019-01-19 19:34")
            };
            Animal a6 = new Animal
            {
                Description = "The gray wolf is one of the world\'s best-known and most-researched animals, with probably more books written about it than any other wildlife species. It has a long history of association with humans, having been despised and hunted in most pastoral communities because of its attacks on livestock, while conversely being respected in some agrarian and hunter-gatherer societies. Although the fear of wolves is pervasive in many human societies, the majority of recorded attacks on people have been attributed to animals suffering from rabies.",
                Family = f2,
                Name = "Wolves",
                PostedOn = DateTime.Parse("2019-01-19 19:36"),
                IsNew = true
            };
            Animal a7 = new Animal
            {
                Description = "A hedgehog is any of the spiny mammals of the subfamily Erinaceinae, in the eulipotyphlan family Erinaceidae. There are seventeen species of hedgehog in five genera, found through parts of Europe, Asia, and Africa, and in New Zealand by introduction. There are no hedgehogs native to Australia, and no living species native to the Americas (the extinct genus Amphechinus was once present in North America). Hedgehogs share distant ancestry with shrews (family Soricidae), with gymnures possibly being the intermediate link, and have changed little over the last 15 million years. Like many of the first mammals, they have adapted to a nocturnal way of life.",
                Family = f3,
                Name = "Hedgehogs",
                PostedOn = DateTime.Parse("2019-01-19 19:37"),
                IsNew = true
            };
            dbContext.Animals.AddRange(a1, a2, a3, a4, a5, a6, a7);
            dbContext.SaveChanges();
            Image i1 = new Image
            {
                Animal = a1,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "fox.png",
                Title = "Fox running in snow"
            };
            Image i2 = new Image
            {
                Animal = a2,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "parrot.png",
                Title = "Orange parrot"
            };
            Image i3 = new Image
            {
                Animal = a3,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "owl.png",
                Title = "Owl is surprised"
            };
            Image i4 = new Image
            {
                Animal = a4,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "squirrel.png",
                Title = "Squirrel in winter"
            };
            Image i5 = new Image
            {
                Animal = a5,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "bear.png",
                Title = "Bear and mountains"
            };
            Image i6 = new Image
            {
                Animal = a6,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "wolf.png",
                Title = "Wolf"
            };
            Image i7 = new Image
            {
                Animal = a7,
                DateTime = DateTime.Parse("2019-01-01 00:00"),
                Path = "hedgehog.png",
                Title = "Hedgehog hiding in grass"
            };
            dbContext.Images.AddRange(i1, i2, i3, i4, i5, i6, i7);
            dbContext.SaveChanges();
        }
    }
}
